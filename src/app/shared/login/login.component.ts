import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginPost } from '../../employee/interfaces/Login.Interface';
import { EmployeeService } from '../../employee/service/employee.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  email: string | undefined;
  password: string | undefined;

  InfoComplete: boolean = true;
  InfoMessage: string = '';

  constructor(private employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
    let validation = this.employeeService.getToken();
    if (validation != '') this.router.navigateByUrl('/home');
  }

  login() {
    this.InfoMessage = '';

    if (this.email && this.password) {
      this.InfoComplete = true;
      const user: LoginPost = { username: this.email, password: this.password };

      this.employeeService.getlogin(user).subscribe(
        (login) => {
          this.employeeService.setToken(login);
          this.router.navigateByUrl('/home');
          // console.log(JSON.parse(this.lstService.getToken()));
        },
        (err) => {
          this.InfoMessage = 'Invalid user or password';
          this.InfoComplete = false;
        }
      );
    } else {
      this.InfoMessage = 'Email or password is empty';
      this.InfoComplete = false;
    }
  }
}

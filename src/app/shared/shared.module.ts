import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { EditEmployeeComponent } from './edit-employee/edit-employee.component';

@NgModule({
    declarations: [
        LoginComponent,
        HomeComponent,
        CreateEmployeeComponent,
        EditEmployeeComponent
    ],
    exports: [],
    imports: [CommonModule, RouterModule, FormsModule],
})
export class SharedModule { }
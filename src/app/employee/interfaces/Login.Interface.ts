export interface LoginPost {

    username: string;
    password: string
}


export interface LoginResponse {
    success: boolean;
    token: string;
}

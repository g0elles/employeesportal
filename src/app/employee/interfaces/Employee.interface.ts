export interface EmployeeResponse {
    id: number;
    firstName: string;
    middleName: string;
    surname: string;
    secondSurname: string;
    country: string;
    identificationType: string;
    identification: string;
    email: string;
    area: string;
    status: boolean;
    entryDate: Date;
}

export interface EmployeePost {
    area: string;
    country: string;
    firstName: string;
    identification: string;
    identificationType: string;
    status: boolean;
    surname: string;
    middleName: string;
    secondSurname: string;
    registryDate: Date;
    entryDate: Date
}

export interface EmployeePut {
    area: string;
    country: string;
    email: string;
    firstName: string;
    id: number;
    identification: string;
    identificationType: string;
    status: boolean;
    surname: string;
    middleName: string;
    secondSurname: string;
    actualizationDate: string;
    entryDate: Date
}
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

import { LoginPost, LoginResponse } from '../interfaces/Login.Interface';
import { EmployeePost, EmployeePut, EmployeeResponse } from '../interfaces/Employee.interface';

@Injectable({
    providedIn: 'root',
})
export class EmployeeService {

    public serviceUrl: string = 'https://localhost:7145/api';
    // private fieldsTemplates: TemplateResponse = { pset: {pset_id:0,pset_name:'',user_id:0} ,props: [] };


    constructor(private http: HttpClient, private cookies: CookieService) { }

    get accessToken(): LoginResponse {
        return JSON.parse(this.cookies.get('token'));
    }

    setToken(token: LoginResponse[]) {
        this.cookies.set('token', JSON.stringify(token));
    }

    getToken() {
        return this.cookies.get('token');
    }

    deleteToken() {
        this.cookies.delete('token');
    }

    getlogin(user: LoginPost): Observable<LoginResponse[]> {
        const url = `${this.serviceUrl}/Login`;
        return this.http.post<LoginResponse[]>(url, user);
    }

    GetEmployees(): Observable<EmployeeResponse[]> {
        const url = `${this.serviceUrl}/Employees/`;

        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.accessToken.token}`
        });

        const requestOptions = { headers: headers };

        return this.http.get<EmployeeResponse[]>(url, requestOptions);

    }

    GetEmployee(Id: Number): Observable<EmployeeResponse[]> {
        const url = `${this.serviceUrl}/Employees/${Id}`;

        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.accessToken.token}`
        });

        const requestOptions = { headers: headers };

        return this.http.get<EmployeeResponse[]>(url, requestOptions);

    }


    CreateEmployee(employee: EmployeePost) {

        const url = `${this.serviceUrl}/Employees/`;

        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.accessToken.token}`
        });

        const requestOptions = { headers: headers };

        return this.http.post(url, employee, requestOptions);

    }


    UpdateEmployee(employee: EmployeePut) {

        const url = `${this.serviceUrl}/Employees/`;

        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.accessToken.token}`
        });

        const requestOptions = { headers: headers };

        return this.http.put(url, employee, requestOptions);

    }


    DeleteEmployee(Id: number) {

        const url = `${this.serviceUrl}/Employee/${Id}`;

        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.accessToken.token}`
        });

        const requestOptions = { headers: headers };

        return this.http.delete(url, requestOptions);

    }
}